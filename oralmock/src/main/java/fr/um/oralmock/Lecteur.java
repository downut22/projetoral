package fr.um.oralmock;

import java.util.ArrayList;

public class Lecteur extends Personne {
	
	private ArrayList<Livre> livresLus;
	
	private ArrayList<Livre> livresAimes;
	
	
	//Getters
	
	public ArrayList<Livre> getLivresLus(){return livresLus;}
	
	public ArrayList<Livre> getLivresAimes(){return livresAimes;}
	
	public ArrayList<Livre> getLivresNonAimes()
	{
		ArrayList<Livre> res = new ArrayList<Livre>();
		
		for(Livre l:livresLus)
		{
			if(!livresAimes.contains(l))
			{
				res.add(l);
			}
		}
		
		return res;
	}
	
	//Setters
	
	public boolean addLivreLus(Livre l)
	{
		if(livresLus.contains(l)) {return false;}
		
		livresLus.add(l);
		l.addLecteur(this);
		
		return true;
	}
	
	public void addLivreAime(Livre l) throws LivreNonLuException
	{
		if(livresLus.contains(l)) 
		{
			livresAimes.add(l);
			l.addLecteursSatisfait(this);
		}
		else {
			throw new LivreNonLuException();
		}
	}
	
	public void removeLivreAime(Livre l)
	{
		if(livresAimes.contains(l))
		{
			livresAimes.remove(l);
			l.removeLecteursSatisfait(this);
		}
	}
	

	public ArrayList<Categorie> getCategorieAimees(){
		ArrayList<Categorie> res = new ArrayList<Categorie>();
		
		for(Livre l:livresAimes)
		{
			if(!res.contains(l.getCategorie()))
			{
				res.add(l.getCategorie());
			}
		}
		
		return res;
	}
	
	//Constructors

	public Lecteur(String name, String nom,ArrayList<Livre> livresLus, ArrayList<Livre> livresAimes) {
		super(name,nom,0);
		this.livresLus = livresLus;
		this.livresAimes = livresAimes;
	}

	public Lecteur(String name, String nom) {
		this(name,nom,new ArrayList<Livre>(),new ArrayList<Livre>());
	}
}
