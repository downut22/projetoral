package fr.um.oralmock;

public class Personne {

	private String name;
	private String nom;
	private int age;
	
	public Personne(String name, String nom, int age) {
		this.name = name;
		this.nom = nom;
		this.age = age;
	}
	
}
