package fr.um.oralmock;

public enum Categorie {
	HISTOIRE, FANTASTIQUE, SF, BIOGRAPHIE, SCIENCE, ENFANT;
	
	// ici une méthode statique non-implémentée que l'on va simuler dans nos tests
	// la méthode renvoie la catégorie correspondante au String passé en paramètre
	public static Categorie stringToCategorie(String nom) {
		throw new UnsupportedOperationException();
	}
}
