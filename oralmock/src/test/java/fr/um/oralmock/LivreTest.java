package fr.um.oralmock;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.condition.DisabledOnOs;
import static org.junit.jupiter.api.condition.OS.*;

public class LivreTest {
	
	Livre livre1;
	Livre livre2;
	Livre livre3;
	
	@BeforeEach
	void init_livres() {
		livre1 = new Livre("1","Au Soleil Levant",20,Categorie.HISTOIRE);
		livre2 = new Livre("2","Le Prince Patate",20,Categorie.ENFANT);
		livre3 = new Livre("3","L'art de la Paix",20,Categorie.HISTOIRE);
	}
	
	@AfterEach
	void delete_livres() {
		Livre.resetLivres();
	}
	
	@Test
	public void testTousLesLivres() {
		delete_livres();
		init_livres();
		assertTrue(Livre.TousLesLivres.contains(livre1));
		assertTrue(Livre.TousLesLivres.contains(livre2));
		assertTrue(Livre.TousLesLivres.contains(livre3));
	}
	
	@Test
	public void testResetLivres() {
		delete_livres();
		init_livres();
		Livre.resetLivres();
		assertFalse(Livre.TousLesLivres.contains(livre1));
		assertFalse(Livre.TousLesLivres.contains(livre2));
		assertFalse(Livre.TousLesLivres.contains(livre3));
	}
	
	@Test
	public void testSiecleValide() {
		delete_livres();
		init_livres();
		assertTrue(Livre.siecleValide(10));
		assertTrue(Livre.siecleValide(-10));
	}
	
	@Test
	public void testSetSiecle() {
		assertTrue(livre1.setSiecle(50).equals("le siècle n'est pas valide") && livre1.getSiecle() == 20);
		assertTrue(livre1.setSiecle(15).equals("siècle ajouté") && livre1.getSiecle() == 15);
	}
	
	
	@Test
	@DisabledOnOs(LINUX)
	public void testGetIsbn() {
		assertEquals(livre1.getIsbn(), "1");
		assertEquals(livre2.getIsbn(), "2");
		assertEquals(livre3.getIsbn(), "3");
	}
	
	
	
}