package fr.um.oralmock;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class LecteurTests {

	Lecteur lecteur ;
	
	@BeforeEach
	void initLecteur() {
		lecteur = new Lecteur("Jean Pascal","Du Nord");
	}
	
	//On teste si l'ajout d'un Livre à la liste des livres lus par un lecteur fonctionne
	@Test
	void TestAddLivreLu()
	{
		Livre livre = new Livre("ISBN","Les ISBNs",20,Categorie.SCIENCE);
	
		lecteur.addLivreLus(livre);
		
		assertTrue(lecteur.getLivresLus().contains(livre));
	}
	
	//On teste si l'ajout d'un Livre à la liste des livres aimes par un lecteur fonctionne s'il a déjà lu le Livre
	@Test
	void TestAddLivreAimeLu()
	{
		Livre livre = new Livre("ISBN","Les ISBNs",20,Categorie.SCIENCE);
	
		lecteur.addLivreLus(livre);
		
		try {
			lecteur.addLivreAime(livre);
		} catch (LivreNonLuException e) {fail();}	//Si l'ajout ne fonctionne pas, ce test echoue
		
		assertTrue(lecteur.getLivresAimes().contains(livre));
	}

	//On teste si l'ajout d'un Livre à la liste des livres aimes par un lecteur renvoie bien une exception s'il n'a pas lu le Livre
	@Test
	void TestAddLivreAimeNonLu()
	{
		Livre livre = new Livre("ISBN","Les ISBNs",20,Categorie.SCIENCE);		
	
		assertThrows(LivreNonLuException.class,() -> lecteur.addLivreAime(livre));
	}
	
	static Stream<Arguments> arguments(){
		return Stream.of(
			  Arguments.of(new Livre("ISBN1", "Les Fleurs", 0, Categorie.FANTASTIQUE)),
			  Arguments.of(new Livre("ISBN2", "Monsieur Soleil", 0, Categorie.BIOGRAPHIE)),
			  Arguments.of(new Livre("ISBN3", "Les Supernovas", 0, Categorie.SCIENCE))
			);
	}
	
	//On teste si l'ajout getCategorieAime renvoie bien un tableau contenant le Livre aimé ajouté
	@ParameterizedTest(name="Categorie du Livre {0}")
	@MethodSource("arguments")
	void TestGetCategoriesAimes(Livre livre) throws LivreNonLuException
	{
		lecteur.addLivreLus(livre);
		lecteur.addLivreAime(livre);
		
		assertArrayEquals(lecteur.getCategorieAimees().toArray(),new Categorie[] {livre.getCategorie()});
	}
}
