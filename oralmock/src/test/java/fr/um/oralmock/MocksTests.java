package fr.um.oralmock;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class MocksTests {
	
	Livre livre0;
	Livre livre2;
	Livre livre3;
	Livre livre4;
	
	@Mock
	Livre livreMock;
	
	Lecteur lecteur0;
	

	Lecteur lecteur1;
	
	Lecteur lecteur2;
	

	@Spy
	Lecteur lecteur3 = new Lecteur("Breno","Durand");;

	@BeforeEach
	void init_livres() {
		 livre0 = new Livre("0","Au Soleil Levant",20,Categorie.HISTOIRE);
		 livre3 = new Livre("3","Le Prince Patate",20,Categorie.ENFANT);
		 livre4 = new Livre("4","L'art de la Paix",20,Categorie.HISTOIRE);
	}
	
	@BeforeEach
	void init_lecteurs()
	{
		lecteur0 = new Lecteur("Patrick","Thepaut");
		lecteur1 = new Lecteur("Arthur","Blaise");
		lecteur2 = new Lecteur("Lea","Villaroya");
	}
	
	@Test
	void TestAddLivre()
	{
		// un test junit basique qui vérifie que la méthode fonctionne correctement
		lecteur0.addLivreLus(livre0);
		
		assertTrue(lecteur0.getLivresLus().contains(livre0));
		assertTrue(livre0.getLecteurs().contains(lecteur0));
	}

	@Test
	void TestAddAndRemoveLivreAime()
	{
		lecteur1.addLivreLus(livreMock);
		try {lecteur1.addLivreAime(livreMock);} catch (LivreNonLuException e) {fail();}
		
		//La methode removeLecteurSatisfait n'est pas développée ; On utilise un mock de Livre pour simuler le comportement attendu de cette methode
		
		ArrayList<Lecteur> arr = new ArrayList<Lecteur>();
		doNothing().when(livreMock).removeLecteursSatisfait(lecteur1); // on utilise ici la méthode doNothing() car notre méthode ne renvoie rien, elle modifie quelque chose
		livreMock.setLecteursSatisfaits(arr);
		
		lecteur1.removeLivreAime(livreMock);
				
		assertFalse(livreMock.getLecteursSatisfaits().contains(lecteur1));
	}

	//On teste si la création de livre en utilisant un Catégorie string assigne bien la Catégorie enum correspondante
	@Test
	void TestLivreFromStringCategorie()
	{	
		// ici on test une méthode statique de notre énumération Categorie, la syntaxe est donc un peu différente
		MockedStatic<Categorie> c = Mockito.mockStatic(Categorie.class);
		
		c.when(()->Categorie.stringToCategorie("FANTASTIQUE")).thenReturn(Categorie.FANTASTIQUE);
		
		livre2 = new Livre("15","Les Hibous",15,"FANTASTIQUE");
		
		//assertTrue(livre2.getCategorie() == Categorie.FANTASTIQUE);
		assertSame(livre2.getCategorie(), Categorie.FANTASTIQUE);
	}

	@Test
	void TestLivreRecommandes() throws LivreNonLuException
	{		 
		Livre.resetLivres();
		Livre l1 = new Livre("115","Les Magiciens",15,Categorie.FANTASTIQUE);
		Livre l2 = new Livre("116","Jean Pascal",15,Categorie.BIOGRAPHIE);
		Livre l3 = new Livre("117","Les Magiciennes",15,Categorie.FANTASTIQUE);
		Livre l4 = new Livre("118","Les Romains",15,Categorie.HISTOIRE);
		Livre l5 = new Livre("119","Seigneur des zeros",15,Categorie.FANTASTIQUE);
		Livre l6 = new Livre("120","L'administration de la fac",15,Categorie.FANTASTIQUE);
		Livre l7 = new Livre("121","La vie des profs",15,Categorie.BIOGRAPHIE);
		
		lecteur3.addLivreLus(l1);
		lecteur3.addLivreLus(l2);
		lecteur3.addLivreLus(l3);
		lecteur3.addLivreLus(l4);
		
		lecteur3.addLivreAime(l1);
		lecteur3.addLivreAime(l2);
		
		ArrayList<Categorie> arr = new ArrayList<Categorie>();
		arr.add(Categorie.FANTASTIQUE);
		arr.add(Categorie.BIOGRAPHIE);

		doReturn(arr).when(lecteur3).getCategorieAimees();

		ArrayList<Categorie> catLoved = new ArrayList<Categorie>();
		catLoved.add(Categorie.FANTASTIQUE);
		catLoved.add(Categorie.BIOGRAPHIE);
		
		when(lecteur3.getCategorieAimees()).thenReturn(catLoved);
	
		ArrayList<Livre> recoExcepted = new ArrayList<Livre>();
		recoExcepted.add(l1);
		recoExcepted.add(l2);
		recoExcepted.add(l3);
		recoExcepted.add(l5);
		recoExcepted.add(l6);
		recoExcepted.add(l7);
		
		ArrayList<Livre> recoGot = Livre.recommandations(lecteur3);
	
		assertArrayEquals(recoGot.toArray(),recoExcepted.toArray());
	}

	
}
